var getJwt = function() {
    return localStorage.getItem('jwt');
};

if (getJwt()) {
    window.location.href = '/dashboard';
}