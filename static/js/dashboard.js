var DATASTORE = {};

var ideasList;
var addIdeaContainer;
$(document).ready(function() {
    ideasList = $('#ideas-list');
    addIdeaContainer = $('#add-idea-container');

    if (!getJwt()) {
        alert('Login to view this page');
        window.location.href = '/login';
        return;
    }
    var userBase64 = getJwt().split('.')[1];
    var user = JSON.parse(window.atob(userBase64));
    var expired = user.exp < parseInt(Date.now() / 1000);
    if (expired) {
        keepTokenAlive()
            .done(function() {
                initialize();
            })
            .fail(function(jqXHR) {
                alert('Please login again' + jqXHR.responseText);
                window.location.href = '/login';
            });
    } else {
        initialize();
    }
});

var initialize = function() {
    $.when(keepTokenAlive()).then(function() {
        $.ajax({
            url: '/me/',
            type: 'GET',
            headers: {
                'x-access-token': getJwt()
            }
        }).done(function (userProfile) {
            $('#user-alias').text(userProfile.name ? userProfile.name : 'User');
            $('#user-image').attr('src', userProfile.avatar_url);
            $('#user-alias-container').removeClass('hidden');
        });
    });

    $.when(keepTokenAlive()).then(function() {
        $.ajax({
            url: '/ideas/',
            type: 'GET',
            headers: {
                'x-access-token': getJwt()
            }
        }).done(function (data) {
            _.each(data.results, function(idea) {
                DATASTORE[idea.id] = idea;
                var ideaListItemTemplate = _.template($('#IdeaListItemTemplate').html());
                ideasList.append(ideaListItemTemplate(idea));
                registerListIdeaListeners();
            });
        });
    });

    $('#logout').click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        $.ajax({
            url: '/access-tokens/',
            type: 'DELETE',
            data: JSON.stringify({
                'refresh_token': localStorage.getItem('refresh')
            }),
            contentType: 'application/json',
            dataType: 'json',
            headers: {
                'x-access-token': getJwt()
            }
        }).always(function() {
            localStorage.clear();
            window.location.href = '/';
        });
    });

    $('#add-idea').click(function (event) {
        event.preventDefault();
        event.stopPropagation();

        var ideaEditTemplate = _.template($('#IdeaEditTemplate').html());

        addIdeaContainer.html(ideaEditTemplate({
            id: null,
            content: null,
            impact: null,
            ease: null,
            confidence: null
        }));

        registerAddIdeaListeners();
    });
};

var registerAddIdeaListeners = function() {
    addIdeaContainer.find('.idea-cancel').unbind().click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        addIdeaContainer.html('');
    });

    addIdeaContainer.find('.idea-submit').unbind().click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var content = addIdeaContainer.find('.idea-content').val();
        var impact = parseInt(addIdeaContainer.find('.idea-impact').val());
        var ease = parseInt(addIdeaContainer.find('.idea-ease').val());
        var confidence = parseInt(addIdeaContainer.find('.idea-confidence').val());

        $.when(keepTokenAlive()).then(function() {
            $.ajax({
                url: '/ideas/',
                type: 'POST',
                data: JSON.stringify({
                    content: content,
                    impact: impact,
                    ease: ease,
                    confidence: confidence,
                }),
                contentType: 'application/json',
                dataType: 'json',
                headers: {
                    'x-access-token': getJwt()
                }
            }).done(function(data) {
                DATASTORE[data.id] = data;
                var ideaListItemTemplate = _.template($('#IdeaListItemTemplate').html());
                ideasList.prepend(ideaListItemTemplate(data));
                addIdeaContainer.html('');
                registerListIdeaListeners();
            }).fail(function(jqXHR) {
                alert('Failed in saving idea' + jqXHR.responseText);
            });
        });
    });
};

var registerListIdeaListeners = function() {
    ideasList.find('.idea-delete').unbind().click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var container = $(event.target).closest("[data-id]");
        var id = container.attr('data-id');

        $.when(keepTokenAlive()).then(function() {
            $.ajax({
                url: '/ideas/' + id + '/',
                type: 'DELETE',
                headers: {
                    'x-access-token': getJwt()
                }
            }).done(function () {
                delete DATASTORE[id];
                ideasList.find('div[data-id="' + id + '"]').remove();
            }).fail(function (jqXHR) {
                alert('Failed deleting idea' + jqXHR.responseText);
            });
        });
    });

    ideasList.find('.idea-edit').unbind().click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var container = $(event.target).closest("[data-id]");
        var id = container.attr('data-id');
        var ideaEditTemplate = _.template($('#IdeaEditTemplate').html());
        $(ideaEditTemplate(DATASTORE[id])).insertBefore(ideasList.find('div[data-id="' + id + '"]'));
        container.remove();
        registerEditIdeaListeners();
    });
};

var registerEditIdeaListeners = function() {
    ideasList.find('.idea-cancel').unbind().click(function(event) {
        event.preventDefault();
        event.stopPropagation();

        var container = $(event.target).closest("[data-id]");
        var id = container.attr('data-id');

        var ideaListItemTemplate = _.template($('#IdeaListItemTemplate').html());
        $(ideaListItemTemplate(DATASTORE[id])).insertBefore(ideasList.find('div[data-id="' + id + '"]'));
        registerListIdeaListeners();
        container.remove();
    });

    ideasList.find('.idea-submit').unbind().click(function(event) {
         event.preventDefault();
        event.stopPropagation();

        var container = $(event.target).closest("[data-id]");
        var id = container.attr('data-id');
        var content = container.find('.idea-content').val();
        var impact = parseInt(container.find('.idea-impact').val());
        var ease = parseInt(container.find('.idea-ease').val());
        var confidence = parseInt(container.find('.idea-confidence').val());

        $.when(keepTokenAlive()).then(function() {
            $.ajax({
                url: '/ideas/' + id + '/',
                type: 'PUT',
                data: JSON.stringify({
                    content: content,
                    impact: impact,
                    ease: ease,
                    confidence: confidence,
                }),
                contentType: 'application/json',
                dataType: 'json',
                headers: {
                    'x-access-token': getJwt()
                }
            }).done(function (data) {
                DATASTORE[data.id] = data;

                var ideaListItemTemplate = _.template($('#IdeaListItemTemplate').html());
                $(ideaListItemTemplate(DATASTORE[id])).insertBefore(ideasList.find('div[data-id="' + id + '"]'));
                registerListIdeaListeners();
                container.remove();
            }).fail(function (jqXHR) {
                alert('Failed in saving idea' + jqXHR.responseText);
            });
        });
    });
};

var keepTokenAlive = function() {
    var userBase64 = getJwt().split('.')[1];
    var user = JSON.parse(window.atob(userBase64));
    var expired = user.exp < parseInt(Date.now() / 1000);
    if (expired) {
        return $.ajax({
            url: 'access-tokens/refresh/',
            type: 'POST',
            data: JSON.stringify({
                "refresh_token": localStorage.getItem('refresh')
            }),
            contentType: 'application/json',
            dataType: 'json',
        }).then(function(data) {
            localStorage.setItem('jwt', data.jwt);
        });
    } else {
        return $.Deferred().resolve();
    }
};

var getJwt = function() {
    return localStorage.getItem('jwt');
};