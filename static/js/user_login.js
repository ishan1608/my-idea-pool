$(document).ready(function() {
    $('#submit').click(function (event) {
        event.preventDefault();
        event.stopPropagation();

        var userEmail = $('#user-email').val();
        var userPassword = $('#user-password').val();

        if (!userEmail) {
            return alert('Email is required');
        }
        if (!userPassword) {
            return alert('Password is required');
        }

        $.ajax({
            url: '/access-tokens/',
            type: 'POST',
            data: JSON.stringify({
                email: userEmail,
                password: userPassword,
            }),
            contentType: 'application/json',
            dataType: 'json',
        }).done(function (data) {
            localStorage.setItem('jwt', data.jwt);
            localStorage.setItem('refresh', data.refresh);
            window.location.href = '/dashboard';
        }).fail(function (jqXHR) {
            alert('Error Registering\n' + jqXHR.responseText);
        });
    });
});