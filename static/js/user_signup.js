$(document).ready(function () {
    $('#submit').click(function (event) {
        event.preventDefault();
        event.stopPropagation();

        var userName = $('#user-name').val();
        var userEmail = $('#user-email').val();
        var userPassword = $('#user-password').val();

        if (!userName) {
            return alert('Name is required');
        }
        if (!userEmail) {
            return alert('Email is required');
        }
        if (!userPassword) {
            return alert('Password is required');
        }

        $.ajax({
            url: '/users/',
            type: 'POST',
            data: JSON.stringify({
                name: userName,
                email: userEmail,
                password: userPassword,
            }),
            contentType: 'application/json',
            dataType: 'json',
        }).done(function (data) {
            localStorage.setItem('jwt', data.jwt);
            localStorage.setItem('refresh', data.refresh);
            window.location.href = '/dashboard';
        }).fail(function (jqXHR) {
            alert('Error Registering\n' + jqXHR.responseText);
        });
    });
});