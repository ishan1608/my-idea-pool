from django.conf.urls import url
from django.urls import include

from .api import router
from .views import user_signup_view, user_login_logout_view, user_token_refresh_view, UserProfileView
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index_view'),
    url(r'^login$', TemplateView.as_view(template_name='login.html'), name='login_view'),
    url(r'^dashboard$', TemplateView.as_view(template_name='dashboard.html'), name='dashboard_view'),
    url(r'^users/$', user_signup_view, name='user_signup_view'),
    url(r'^access-tokens/$', user_login_logout_view),
    url(r'^access-tokens/refresh/$', user_token_refresh_view),
    url(r'^me/$', UserProfileView.as_view()),
    url(r'^', include(router.urls)),
]
