import json

from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from libgravatar import Gravatar
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .models import User


@csrf_exempt
def user_signup_view(request, **kwargs):
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])

    try:
        payload = json.loads(request.body)
    except json.decoder.JSONDecodeError:
        return HttpResponseBadRequest('Invalid Payload')

    email = payload.get('email')
    name = payload.get('name')
    password = payload.get('password')
    if not email or not password or not name:
        return HttpResponseBadRequest('Email, Password and Name are mandatory fields')

    if User.objects.filter(email=email).first():
        return HttpResponseBadRequest('Email already registered')

    user = User.objects.create(email=email)
    user.first_name = name
    user.set_password(password)
    user.save()
    login(request, user)

    token_pair = TokenObtainPairView.as_view()(request, **kwargs).data
    token_pair['jwt'] = token_pair['access']
    del token_pair['access']
    return HttpResponse(json.dumps(token_pair), content_type='application/json')


@csrf_exempt
def user_login_logout_view(request, **kwargs):
    if request.method not in ['POST', 'DELETE']:
        return HttpResponseNotAllowed(['POST', 'DELETE'])

    try:
        payload = json.loads(request.body)
    except json.decoder.JSONDecodeError:
        return HttpResponseBadRequest('Invalid Payload')

    if request.method == 'POST':
        email = payload.get('email')
        password = payload.get('password')
        if not email or not password:
            return HttpResponseBadRequest('Email and Password are mandatory fields')

        user = authenticate(**payload)
        if not user:
            return HttpResponse('Email and Password incorrect', status=401)

        token_pair = TokenObtainPairView.as_view()(request, **kwargs).data
        token_pair['jwt'] = token_pair['access']
        del token_pair['access']
        return HttpResponse(json.dumps(token_pair), content_type='application/json')

    if request.method == 'DELETE':
        refresh_token = payload.get('refresh_token')
        if not refresh_token:
            return HttpResponseBadRequest('Refresh Token is a mandatory field')

        token = RefreshToken(refresh_token)
        token.blacklist()
        return HttpResponse(status=204)


@csrf_exempt
def user_token_refresh_view(request, **kwargs):
    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])

    try:
        payload = json.loads(request.body)
    except json.decoder.JSONDecodeError:
        return HttpResponseBadRequest('Invalid Payload')

    refresh_token = payload.get('refresh_token')
    if not refresh_token:
        return HttpResponseBadRequest('Refresh Token is a mandatory field')

    request._body = bytes(json.dumps({'refresh': refresh_token}), 'utf-8')
    refreshed_token = TokenRefreshView.as_view()(request, **kwargs).data
    refreshed_token['jwt'] = refreshed_token['access']
    del refreshed_token['access']
    return HttpResponse(json.dumps(refreshed_token), content_type='application/json')


class UserProfileView(APIView):
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        return Response({
            'email': request.user.email,
            'name': request.user.first_name,
            'avatar_url': Gravatar(request.user.email).get_image()
        }, 200)
