from django.db.models import ExpressionWrapper, FloatField, F
from rest_framework import serializers, viewsets, routers

from .models import Idea


class IdeaSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = Idea
        fields = ('id', 'content', 'impact', 'ease', 'confidence', 'user')


class IdeaViewSet(viewsets.ModelViewSet):
    queryset = Idea.objects.all()
    serializer_class = IdeaSerializer

    def filter_queryset(self, queryset):
        queryset = (
            queryset.filter(user=self.request.user)
                    .annotate(avg=ExpressionWrapper((F('ease') + F('impact') + F('confidence')) / 3.0, output_field=FloatField()))
                    .order_by('-avg', '-id')
        )
        return super().filter_queryset(queryset)


router = routers.DefaultRouter()
router.register(r'ideas', IdeaViewSet)
